##################
#
# VPC variable definition
#
##################

variable "vpcCIDRblock" {
    description = "CIDR for the VPC"
    type        = string
    default     = "10.0.0.0/19"
}

variable "dnsSupport" {
   description = "Should be true to enable DNS support in the Default VPC"
   type        = bool
   default     = true
}

variable "dnsHostNames" {
   description = "Should be true to enable DNS hostnames in the Default VPC"
   type        = bool
   default     = true
}

variable "instanceTenancy" {
   description = "A tenancy option for instances launched into the VPC"
   type        = string
   default     = "default"
    
}

variable "vpc_name"{
  description = "name of vpc"
  type        = string
  default     = "Pomelo VPC"
}

variable "vpc_creator"{
 description = "creator user name of resource"
 type        = string
 default     = "PRO"
}
variable "vpc_region"{
 description = "resource of region"
 type        = string
 default     = "us-east-1"
}
variable "vpc_env"{
 description = "environment for resource"
 type        = string
 default     = "dev"
}
variable "vpc_atomation"{
 type        = string
 default     = "terraform"
}
variable "account_id"{
 description = "account id for resource"
 type        = string
 default     = "007"
}


################
# Variable Public subnet
################

variable "publicsubnetCIDRblock" {
    description = "CIDR block for subnet"
    type        = string
    default     = "10.0.1.0/24"
}

variable "mapPublicIP" {
   description = "Should be false if you do not want to auto-assign public IP on launch"
   type        = bool
   default     = true
}

variable "availabilityZone" {
    description = "Availability zones names or ids in the region"
    type        = string
    default     = "us-east-1a"
}

variable "public_subnet_name" {
  description = "tagging for the subnet"
  type        = string
  default     = "Pomelo_public_subnet"
}
