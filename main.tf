module "vpc" {
  source = "./modules"

  vpc_name = "Pomelovpc"
  vpc_cidr = "10.0.0.0/16"
}
